using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Castillo : MonoBehaviour
{
    public Transform jugador;
    public Transform sol;
    public Transform RotDia;
    public Transform RotNoc;
    public float Distancia;
    public float interpolacion;
    public GameObject lluvia1, lluvia2;
    void Update()
    {
        Distancia = Vector3.Distance(jugador.position, transform.position);
        interpolacion = Mathf.InverseLerp(5, 13, Distancia);

        Quaternion rotacion=Quaternion.Lerp(RotNoc.rotation, RotDia.rotation, interpolacion);
        sol.rotation = rotacion;
        if (interpolacion < 1)
        {
            lluvia1.SetActive(true);
            lluvia2.SetActive(true);
            AudiManager.instance.PlayAudio(AudiManager.instance.BGMusica);
        }
        else
        {
            lluvia1.SetActive(false);
            lluvia1.SetActive(false);
        }
    }
}
