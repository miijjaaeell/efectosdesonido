using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoAleatorio : MonoBehaviour
{
    AudioSource sonido;
    // Start is called before the first frame update
    void Awake()
    {
        sonido = GetComponent<AudioSource>();
    }
    IEnumerator Start()
    {
        while (true)
        {
            int segs = Random.Range(5,10);
            Debug.Log(segs);
            yield return new WaitForSeconds(segs);
            sonido.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
