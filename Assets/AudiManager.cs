using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudiManager : MonoBehaviour
{
    public AudioMixer musicMixer, effectsMixer;
    public AudioSource forest, publico, medieval, bird, BGMusica;

    public static AudiManager instance;
    public float masterVol, effectVol;
    private void Awake()
    {
        if (instance == null)
        {
           instance = this;
        }
    }
    void Update()
    {
        MasterVolumen();
        EffectsVolumen();
    }
    public void MasterVolumen()
    {
        musicMixer.SetFloat("masterVolumen", masterVol);
    }
    public void EffectsVolumen()
    {
        musicMixer.SetFloat("effectsVolumen", effectVol);
    }

    public void PlayAudio(AudioSource audio)
    {
        audio.Play();
    }
}